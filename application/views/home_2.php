<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	$user_login = $this->session->userdata();
 ?>
  
 <head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<title>TOKO JAYA ABADI</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker({dateFormat : "yy-dd-mm"});
	$( "#datepicker1" ).datepicker({dateFormat : "yy-dd-mm"});
	
  } );
  </script>

 
    
 </head>


 
 <body>
  
   <center>
<?php 
	if ($user_login['tipe']== "1"){
		$this->load->view('template/view_menu');
	}else{
		$this->load->view('template/view_menu_user');
	}
	
?>

</center>

<?php $this->load->view($content);?>
</body>