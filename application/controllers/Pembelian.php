<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("supplier_model");
		$this->load->model("pembelian_model");
		$this->load->model("barang_model");
		
		//cek sesi login
		$user_login = $this->session->userdata();
		if(count($user_login) <= 1){
			redirect("auth/index", "refresh");
		}

		$this->load->library("pdf/pdf");
	}
	
	public function index()
	{
		$this->listpembelian();
	}
	
	public function listpembelian()
	{
		$data['data_pembelian'] = $this->pembelian_model->tampilDataPembelian();
		$data['content']	   ='forms/Home_pembelian';
		$this->load->view('home_2', $data);

		
	}
	
	public function input_h()
	{
		$data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
		
		//if (!empty($_REQUEST)) {
			
			//$m_pembelian_h = $this->pembelian_model;
			//$m_pembelian_h->savePembelianHeader();
			//$id_terakhir = array();
			
			//panggil ID transaksi terakhir
			//$id_terakhir = $m_pembelian_h->idTransaksiTerakhir();

			//redirect("pembelian/input_d/" . $id_terakhir, "refresh");$validation = $this->form_validation;
		$validation = $this->form_validation;
		$validation->set_rules($this->pembelian_model->rules());
			
			if ($validation->run()){
				$this->pembelian_model->savePembelianHeader();
				$this->session->set_flashdata('info', '<div style="color: green" align="center"> SIMPAN DATA BERHASIL! </div>');
				redirect("pembelian/index", "refresh");
		}
			
		//$this->load->view('input_karyawan', $data);
		$data['content'] = 'forms/input_pembelian';
			$this->load->view('home_2', $data);
		
		//$this->load->view('input_pembelian', $data);
	}
	
	public function input_d($id_pembelian_header)
	{
		
		$data['id_header']				= $id_pembelian_header;
		$data['data_barang'] 			= $this->barang_model->tampilDataBarang();
		$data['data_pembelian_detail'] 	= $this->pembelian_model->tampilDataPembelianDetail($id_pembelian_header);
		
		//if (!empty($_REQUEST)) {
			
			//save detail
			//$this->pembelian_model->savePembelianDetail($id_pembelian_header);
			//proses update stok
			//$kode_barang	= $this->input->post('kode_barang');
			//$qty			= $this->input->post('qty');
			//$this->barang_model->updateStok($kode_barang, $qty);
			
			//redirect("pembelian/input_d/" . $id_pembelian_header , "refresh");
		$validation = $this->form_validation;
		$validation->set_rules($this->pembelian_model->rules1());
			
			if ($validation->run()){
				$this->pembelian_model->savePembelianDetail($id_pembelian_header);
				$this->session->set_flashdata('info', '<div style="color: green"> SIMPAN DATA BERHASIL! </div>');
				redirect("pembelian/input_d/" . $id_pembelian_header , "refresh");
		}
			
		//$this->load->view('input_karyawan', $data);
		$data['content'] = 'forms/input_pembelian_d';
			$this->load->view('home_2', $data);
		
		
		//$this->load->view('input_pembelian_d', $data);
	}
	public function report()
	{
		$data['content'] 	= 'forms/datepicker';
		$this->load->view('home_2', $data);
}
	


	public function laporanPembelian()
	{    
			// echo "<prev>";
		 // print_r($this->input->post('tgl_awal'));die();
		 //   echo "</prev>";

		 $tgl_awal=$this->input->post('tgl_awal');
		 $tgl_akhir=$this->input->post('tgl_akhir');
		 $data['data_pembelian_detail'] = $this->pembelian_model->tampilLaporanPembelian($tgl_awal, $tgl_akhir);
		$data['tgl_awal'] = $tgl_awal;
		$data['tgl_akhir']= $tgl_akhir;

		$data['content'] ='forms/laporan_pembelian';
		$this->load->view('home_2', $data);
	}

	public function cetak($tgl_awal, $tgl_akhir)
   {
        $pdf = new FPDF('P','mm','A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B' ,15);
        // mencetak string 
        $pdf->Cell(187, 7, 'Loporan Data Pembelian', 0, 1, 'C');
        $pdf->SetFont('Arial','B',15);
        $pdf->Cell(190,7,'TOKO JAYA ABADI',0,1,'C');
           
        // Memberikan space kebawah agar tidak terlalu rapat
       $pdf->Cell(10,10,'',0,1,'L');
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(18, 6, 'No', 1, 0, 'C');
         $pdf->Cell(22, 6, 'Id pembelian', 1, 0, 'C');
        $pdf->Cell(35, 6, 'Nomor transaksi', 1, 0, 'C');
        $pdf->Cell(30,6,'Tanggal',1,0,'C');
       
        $pdf->Cell(30,6,'Total barang',1,0,'C');
        $pdf->Cell(30,6,'Total qty',1,0,'C');
        $pdf->Cell(29,6,'Jumlah nominal',1,1,'C');
         
        $pdf->SetFont('Arial','B',10);
        $no = 0;
        $total_keseluruhan = 0;
        $data_pembelian_detail = $this->pembelian_model->tampilLaporanPembelian($tgl_awal,$tgl_akhir);
       
        foreach ($data_pembelian_detail as $data){
            $no ++;
            $pdf->Cell(18,6,$no,1,0,'C');
            $pdf->Cell(22,6,$data->id_pembelian_h,1,0,'C');
            $pdf->Cell(35,6,$data->no_transaksi,1,0,'C');
            $pdf->Cell(30,6,$data->tanggal,1,0,'C');
            $pdf->Cell(30,6,$data->total_barang,1,0,'C'); 
            $pdf->Cell(30,6,$data->total_qty,1,0,'C'); 
            $pdf->Cell(29,6,'Rp.'. number_format($data->total_pembelian),1,1,'R');

              $total_keseluruhan += $data->total_pembelian; 
        }
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(165,6,'Total Keseluruhan',1,0,'C');
        $pdf->Cell(29,6,'Rp.'. number_format($total_keseluruhan),1,1,'R');
        $pdf->Output();
    }
 

}



