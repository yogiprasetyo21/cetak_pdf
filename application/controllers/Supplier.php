<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("supplier_model");
		
		//cek sesi login
		$user_login = $this->session->userdata();
		if(count($user_login) <= 1){
			redirect("auth/index", "refresh");
		}
	}
	
	public function index()
	{
		$this->listsupplier();
	}
	
	public function listsupplier()
	{
		if (isset($_POST['tombol_cari'])) {
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_pencarian', $data['kata_pencarian']);
		}else{
			$data['kata_pencarian'] = $this->session->userdata('session_pencarian');
			
		}
		
		$data['data_supplier'] = $this->supplier_model->tombolpagination($data['kata_pencarian']);

		//$data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
		$data['content']	   ='forms/Home_supplier';
		$this->load->view('home_2', $data);

		
	}
	
	public function input()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->supplier_model->rules());
			
			if ($validation->run()){
				$this->supplier_model->save();
				$this->session->set_flashdata('info', '<div style="color: green"> SIMPAN DATA BERHASIL! </div>');
				redirect("supplier/index", "refresh");
		}
			
		//$this->load->view('input_karyawan', $data);
		$data['content'] = 'forms/Input_supplier';
			$this->load->view('home_2', $data);
	}
	public function detailsupplier($kode_supplier)
	{
		$data['detail_supplier'] = $this->supplier_model->detail($kode_supplier);
		$data['content'] = 'forms/Detail_supplier';
			$this->load->view('home_2', $data);
	}
	
	public function edit($kode_supplier)
	{
		$data['detail_supplier'] = $this->supplier_model->detail($kode_supplier);
		
		$validation = $this->form_validation;
		$validation->set_rules($this->supplier_model->rules());
			
			if ($validation->run()){
				$this->supplier_model->update($kode_supplier);
				$this->session->set_flashdata('info', '<div style="color: green"> EDIT DATA BERHASIL! </div>');
				redirect("supplier/index", "refresh");
		}
			
		//$this->load->view('input_karyawan', $data);
		$data['content'] = 'forms/Edit_supplier';
			$this->load->view('home_2', $data);

	}
	
	public function delete($kode_supplier)
	{
		$m_supplier = $this->supplier_model;
		$m_supplier->delete($kode_supplier);
		redirect("supplier/index", "refresh");
	}
	
}