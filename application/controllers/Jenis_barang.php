<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_barang extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("jenis_barang_model");
		
		//cek sesi login
		$user_login = $this->session->userdata();
		if(count($user_login) <= 1){
			redirect("auth/index", "refresh");
		}
	}
	
	public function index()
	{
		$this->listjenisbarang();
	}
	
	public function listjenisbarang()
	{
		if (isset($_POST['tombol_cari'])) {
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_pencarian', $data['kata_pencarian']);
		}else{
			$data['kata_pencarian'] = $this->session->userdata('session_pencarian');
			
		}
		
		$data['data_jenis_barang'] = $this->jenis_barang_model->tombolpagination($data['kata_pencarian']);

		//$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDataJenisBarang();
		$data['content']	   ='forms/Home_jenis_barang';
		$this->load->view('home_2', $data);

		
	}
	
	public function input()
	{
		
		$validation = $this->form_validation;
		$validation->set_rules($this->jenis_barang_model->rules());
			
			if ($validation->run()){
				$this->jenis_barang_model->save();
				$this->session->set_flashdata('info', '<div style="color: green" align="center"> SIMPAN DATA BERHASIL! </div>');
				redirect("jenis_barang/index", "refresh");
		}
			
		//$this->load->view('input_karyawan', $data);
		$data['content'] = 'forms/Input_jenis_barang';
			$this->load->view('home_2', $data);
	}
	
	public function detailjenisbarang($kode_jenis)
	{
		$data['detail_jenis_barang'] = $this->jenis_barang_model->detail($kode_jenis);
		$data['content'] = 'forms/Detail_jenis_barang';
		$this->load->view('home_2', $data);
	}
	
	public function edit($kode_jenis)
	{
		$data['detail_jenis_barang'] = $this->jenis_barang_model->detail($kode_jenis);
		
		$validation = $this->form_validation;
		$validation->set_rules($this->jenis_barang_model->rules());
			
			if ($validation->run()){
				$this->jenis_barang_model->update($kode_jenis);
				$this->session->set_flashdata('info', '<div style="color: green"> EDIT DATA BERHASIL! </div>');
				redirect("jenis_barang/index", "refresh");
		}
			
		//$this->load->view('input_karyawan', $data);
		$data['content'] = 'forms/Edit_jenis_barang';
		$this->load->view('home_2', $data);
	}
	
	public function delete($kode_jenis)
	{
		$m_jenis_barang = $this->jenis_barang_model;
		$m_jenis_barang->delete($kode_jenis);
		redirect("jenis_barang/index", "refresh");
	}
	
}