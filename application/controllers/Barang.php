<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("barang_model");
		$this->load->model("jenis_barang_model");
		
		//cek sesi login
		$user_login = $this->session->userdata();
		if(count($user_login) <= 1){
			redirect("auth/index", "refresh");
		}
	}
	
	public function index()
	{
		$this->listbarang();
	}
	
	public function listbarang()
	{
			if (isset($_POST['tombol_cari'])) {
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_pencarian', $data['kata_pencarian']);
		}else{
			$data['kata_pencarian'] = $this->session->userdata('session_pencarian');
			
		}
		
		$data['data_barang'] = $this->barang_model->tombolpagination($data['kata_pencarian']);
 		
		$data['content']       ='forms/Home_barang';
		$this->load->view('home_2', $data);
	}
	
	public function input()
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDataJenisBarang();
		
		$validation = $this->form_validation;
		$validation->set_rules($this->barang_model->rules());
			
			if ($validation->run()){
				$this->barang_model->save();
				$this->session->set_flashdata('info', '<div style="color: green" align="center"> SIMPAN DATA BERHASIL! </div>');
				redirect("barang/index", "refresh");
		}
			
		//$this->load->view('input_karyawan', $data);
		$data['content'] = 'forms/Input_barang';
			$this->load->view('home_2', $data);
	}
	
	public function detailbarang($kode_barang)
	{
		$data['detail_barang'] = $this->barang_model->detail($kode_barang);
		//$this->load->view('input_karyawan', $data);
		$data['content'] = 'forms/Detail_barang';
			$this->load->view('home_2', $data);
	}
	
	public function edit($kode_barang)
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDataJenisBarang();
		$data['detail_barang'] = $this->barang_model->detail($kode_barang);
		
		$validation = $this->form_validation;
		$validation->set_rules($this->karyawan_model->rules());
			
			if ($validation->run()){
				$this->barang_model->update($kode_barang);
				$this->session->set_flashdata('info', '<div style="color: green"> EDIT DATA BERHASIL! </div>');
				redirect("barang/index", "refresh");
		}
			
		//$this->load->view('input_karyawan', $data);
		$data['content'] = 'forms/Edit_barang';
			$this->load->view('home_2', $data);
	}
	
	public function delete($kode_barang)
	{
		$m_barang = $this->barang_model;
		$m_barang->delete($kode_barang);
		redirect("barang/index", "refresh");
	}
	
}