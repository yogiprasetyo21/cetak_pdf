<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("karyawan_model");
		$this->load->model("jabatan_model");
		
		//cek sesi login
		$user_login = $this->session->userdata();
		if(count($user_login) <= 1){
			redirect("auth/index", "refresh");
		}
	}
	
	public function index()
	{
		$this->listkaryawan();
	}
	
	public function listKaryawan()
	
	{
		if (isset($_POST['tombol_cari'])) {
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('s		ession_pencarian', $data['kata_pencarian']);
		}else{
			$data['kata_pencarian'] = $this->session->userdata('session_pencarian');
			
		}
		
		$data['data_karyawan'] = $this->karyawan_model->tombolpagination($data['kata_pencarian']);
 		
		//$data['data_karyawan'] = $this->Karyawan_model->tampilDataKaryawan();
		$data['content']       ='forms/Home_karyawan';
		$this->load->view('home_2', $data);
	}
	
	public function input()
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		$validation = $this->form_validation;
		$validation->set_rules($this->karyawan_model->rules());
			
			if ($validation->run()){
				$this->karyawan_model->save();
				$this->session->set_flashdata('info', '<div style="color: green"> SIMPAN DATA BERHASIL! </div>');
				redirect("karyawan/index", "refresh");
		}
			
		//$this->load->view('input_karyawan', $data);
		$data['content'] = 'forms/Input_karyawan';
			$this->load->view('home_2', $data);
	
	}
	
	public function detailkaryawan($nik)
	{
		$data['detail_karyawan'] = $this->karyawan_model->detail($nik);
		$data['content'] = 'forms/Detail_karyawan';
			$this->load->view('home_2', $data);
	}
	
	public function edit($nik)
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		$data['detail_karyawan'] = $this->karyawan_model->detail($nik);
		
		$validation = $this->form_validation;
		$validation->set_rules($this->karyawan_model->rules());
			
			if ($validation->run()){
				$this->karyawan_model->update($nik);
				$this->session->set_flashdata('info', '<div style="color: green"> EDIT DATA BERHASIL! </div>');
				redirect("karyawan/index", "refresh");
		}
			
		//$this->load->view('input_karyawan', $data);
		$data['content'] = 'forms/Edit_karyawan';
			$this->load->view('home_2', $data);
	
	}
	
	public function delete($nik)
	{
		$m_karyawan = $this->karyawan_model;
		$m_karyawan->delete($nik);
		redirect("karyawan/index", "refresh");
	}
}