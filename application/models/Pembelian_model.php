<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian_model extends CI_Model
{
	//panggil nama table
	private $_table_header = "pembelian_header";
	private $_table_detail = "pembelian_detail";


		public function rules()
{
		return[
		[
				'field' =>'no_transaksi',
				'label' =>'no transaksi',
				'rules' =>'required|max_length[5]',
				'errors' =>[
					'required' => 'no transaksi tidak boleh kosong.',
					'max_length' => 'no transaksi tidak boleh lebih dari 5 karakter.',
				]

		],
		[
				'field' =>'kode_supplier',
				'label' =>'kode supplier',
				'rules' =>'required',
				'errors' =>[
					'required' => 'kode supplier tidak boleh kosong.',
					
				
				]
		        ]
		];
	}


		public function rules1()
{
		return[
		[
				'field' =>'kode_barang',
				'label' =>'kode barang',
				'rules' =>'required|max_length[5]',
				'errors' =>[
					'required' => 'kode barang tidak boleh kosong.',
					'max_length' => 'kode barang tidak boleh lebih dari 5 karakter.',
      	]

		],
		[
				'field' =>'qty',
				'label' =>'qty',
				'rules' =>'required|numeric',
				'errors' =>[
					'required' => 'qty tidak boleh kosong.',
					'numeric' => 'qty harus angka',


					
				]

		],
		[
				'field' =>'harga',
				'label' =>'Harga Barang',
				'rules' =>'required|numeric',
				'errors' =>[
					'required' => 'Harga barang tidak boleh kosong.',
					'numeric' => 'Harga barang harus angka',
					
				
				]
		        ]
		];
	}
	

	
	public function tampilDataPembelian()
	{
		$query = $this->db->query(
			"SELECT * FROM " . $this->_table_header . " WHERE flag = 1" );
		return $query->result();
	
	}
	
		public function tampilLaporanPembelian($tgl_awal, $tgl_akhir)
	{
  $this->db->select("ph.id_pembelian_h, ph.no_transaksi,ph.tanggal, COUNT(pd.kode_barang) AS total_barang, SUM(pd.qty) AS total_qty, SUM(pd.jumlah) AS total_pembelian");

   $this->db->FROM("pembelian_header AS ph"); 
   $this->db->JOIN("pembelian_detail AS pd", "ph.id_pembelian_h = pd.id_pembelian_h");

   $this->db->WHERE("ph.tanggal BETWEEN '$tgl_awal' AND '$tgl_akhir'");
   $this->db->GROUP_BY("ph.id_pembelian_h");
        $query = $this->db->get();
         
        return $query->result();   

    }

	
	public function idTransaksiTerakhir()
	{
		
		$query = $this->db->query(
			"SELECT * FROM " . $this->_table_header . " WHERE flag = 1 ORDER BY
			id_pembelian_h DESC LIMIT 0,1" );
			$data_id = $query->result();
			
			foreach ($data_id as $data){
				$last_id = $data->id_pembelian_h;
			}
		return $last_id;
	}
	
	
	
	public function savePembelianHeader()
	{
		$data['no_transaksi']	= $this->input->post('no_transaksi');
		$data['kode_supplier']	= $this->input->post('kode_supplier');
		$data['tanggal']		= date('Y-m-d');
		$data['approved']		= 1;
		$data['flag']			= 1;
		$this->db->insert($this->_table_header, $data);
	}

	Public function tampilDataPembelianDetail($id_pembelian_header)
	{			
		$query = $this->db->query(
			"SELECT A. *,  B.nama_barang FROM " . $this->_table_detail . " AS A
			INNER JOIN `barang` AS B ON A.kode_barang = B.kode_barang
			WHERE A.`flag` = '1' AND A.`id_pembelian_h`  = ". $id_pembelian_header 
			);
			$data = $query->result();
			
			
		return $query->result();
	}
	
	public function savePembelianDetail($id_pembelian_header)
	{
		$harga = $this->input->post('harga');
		$qty = $this->input->post('qty');
		
		$data['id_pembelian_h']	= $id_pembelian_header;
		$data['kode_barang']	= $this->input->post('kode_barang');
		$data['qty']			= $qty;
		$data['harga']			= $harga;
		$data['jumlah']			= $harga * $qty;
		$data['flag']			= 1;
		$this->db->insert($this->_table_detail, $data);
	}
}
