<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_model extends CI_Model
{
	//panggil nama table
	private $_table = "barang";

	public function rules()
{
		return[
		[
				'field' =>'kode_barang',
				'label' =>'kode barang',
				'rules' =>'required|max_length[10]',
				'errors' =>[
					'required' => 'kode barang tidak boleh kosong.',
					'max_length' => 'kode barang tidak boleh lebih dari 10 karakter.',
				]

		],
		[
				'field' =>'nama_barang',
				'label' =>'Nama Barang',
				'rules' =>'required',
				'errors' =>[
					'required' => 'Nama Barang tidak boleh kosong.',
					
				]

		],
		[
				'field' =>'harga_barang',
				'label' =>'Harga Barang',
				'rules' =>'required|numeric',
				'errors' =>[
					'required' => 'Harga barang tidak boleh kosong.',
					'numeric' => 'Harga barang harus angka',
				]
		],
		[
				'field' =>'kode_jenis',
				'label' =>'kode jenis',
				'rules' =>'required',
				'errors' =>[
					'required' => 'kode jenis tidak boleh kosong.',


				]

		],
		[
				'field' =>'stok',
				'label' =>'stok',
				'rules' =>'required|numeric',
				'errors' =>[
					'required' => 'stok barang tidak boleh kosong.',
					'numeric' => 'stok barang harus angka',					
				]
		        ]
		];
	}
	
	
	public function tampilDataBarang()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	
	}
	
	public function tampilDataBarang2()
	{
		$query = $this->db->query("SELECT * FROM jenis_barang as jb inner join barang as br on
		jb.kode_jenis=br.kode_jenis");
		return $query->result();
	
	}
	
	public function tampilDataBarang3()
	{
		$this->db->select('*');
		$this->db->order_by('kode_barang', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function detail($kode_barang)
	{
		$this->db->select('*');
		$this->db->where('kode_barang', $kode_barang);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function save()
	{
		$data['kode_barang']	= $this->input->post('kode_barang');
		$data['nama_barang']	= $this->input->post('nama_barang');
		$data['harga_barang']	= $this->input->post('harga_barang');
		$data['kode_jenis']		= $this->input->post('kode_jenis');
		$data['flag']			= 1;
		$this->db->insert($this->_table, $data);
	}
	
	public function update($kode_barang)
	{
		$data['nama_barang']	= $this->input->post('nama_barang');
		$data['harga_barang']	= $this->input->post('harga_barang');
		$data['kode_jenis']		= $this->input->post('kode_jenis');
		$data['flag']			= 1;
		$this->db->where('kode_barang', $kode_barang);
		$this->db->update($this->_table, $data);
	}
	
	public function delete($kode_barang)
	{
		//delete from db
		$this->db->where('kode_barang', $kode_barang);
		$this->db->delete($this->_table);
	}
	
	public function updateStok($kode_barang, $qty)
	{
		$cari_stok = $this->detail($kode_barang);
		foreach ($cari_stok as $data){
			$stok = $data->stok;
		}
		
		$jumlah_stok = $stok + $qty;
		$data_barang['stok'] = $jumlah_stok;
		
		$this->db->where('kode_barang', $kode_barang);
		$this->db->update('barang', $data_barang);
	}

  public function tampilDataBarangPagination($perpage, $uri, $data_pencarian)
{
	$this->db->select('barang.kode_barang, barang.nama_barang,
		barang.harga_barang, jenis_barang.nama_jenis,  barang.stok');
	$this->db->join('jenis_barang', 'jenis_barang.kode_jenis=
		barang.kode_jenis');
	if (!empty($data_pencarian)) {
		$this->db->like('barang.nama_barang', $data_pencarian);
		}
		$this->db->order_by('barang.kode_barang','asc');
		
		$get_data = $this->db->get($this->_table, $perpage, $uri);
		if ($get_data->num_rows() > 0) {
			return $get_data->result();
			
			}else{
				return null;
			}	
}

	
	
	
	public function tombolpagination($data_pencarian)
{
	$this->db->like('nama_barang', $data_pencarian);
	$this->db->from($this->_table);
	$hasil = $this->db->count_all_results();
	
	//pagination limt
	$pagination['base_url'] = base_url().'barang/listbarang/load/';
	$pagination['total_rows'] =$hasil;
	$pagination['per_page'] = "3";
	$pagination['uri_segment'] = 4;
	$pagination['num_links'] = 2;
	
	
	$pagination['full_tag_open'] = '<div class="pagination">';
	$pagination['full_tag_close'] = '</div>';
	
	$pagination['first_link'] = 'First Page';
	$pagination['first_tag_open'] = '<span class="firstlink">';
	$pagination['first_tag_close'] = '</span>';
	
	
	$pagination['last_link'] = 'Last Page';
	$pagination['last_tag_open'] = '<span class="lastlink">';
	$pagination['last_tag_close'] = '</span>';
	
	$pagination['next_link'] = 'Next Page';
	$pagination['next_tag_open'] = '<span class="nextlink">';
	$pagination['next_tag_close'] = '</span>';
	
	
	$pagination['prev_link'] = 'Prev Page';
	$pagination['prev_tag_open'] = '<span class="prevlink">';
	$pagination['prev_tag_close'] = '</span>';
	
	
	$pagination['cur_tag_open'] = '<span class="curlink">';
	$pagination['cur_tag_close'] = '</span>';
	
	$pagination['num_tag_open'] = '<span class="numlink">';
	$pagination['num_tag_close'] = '</span>';
	
	$this->pagination->initialize($pagination);
	
	$hasil_pagination = $this->tampilDataBarangPagination($pagination['per_page'],
	$this->uri->segment(4), $data_pencarian);
	
	return $hasil_pagination;
	
	}

}

