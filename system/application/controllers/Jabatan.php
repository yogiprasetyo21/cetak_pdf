<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("jabatan_model");
		
		//cek sesi login
		$user_login = $this->session->userdata();
		if(count($user_login) <= 1){
			redirect("auth/index", "refresh");
		}
	}
	
	public function index()
	{
		$this->listjabatan();
	}
	
	public function listjabatan()
	{
			if (isset($_POST['tombol_cari'])) {
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_pencarian', $data['kata_pencarian']);
		}else{
			$data['kata_pencarian'] = $this->session->userdata('session_pencarian');
			
		}
		
		$data['data_jabatan'] = $this->jabatan_model->tombolpagination($data['kata_pencarian']);
 		
		$data['content']       ='forms/Home_jabatan';
		$this->load->view('home_2', $data);
	}
	
			
	public function input()
	{
		$validation = $this->form_validation;
		$validation->set_rules($this->jabatan_model->rules());
			
			if ($validation->run()){
				$this->jabatan_model->save();
				$this->session->set_flashdata('info', '<div style="color: green"> SIMPAN DATA BERHASIL! </div>');
				redirect("jabatan/index", "refresh");
		}
			
		//$this->load->view('input_karyawan', $data);
		$data['content'] = 'forms/Input_jabatan';
			$this->load->view('home_2', $data);
	}
	
	public function detailjabatan($kode_jabatan)
	{
		$data['detail_jabatan'] = $this->jabatan_model->detail($kode_jabatan);
		$data['content'] = 'forms/Detail_jabatan';
			$this->load->view('home_2', $data);
	}
	
	public function edit($kode_jabatan)
	{
		$data['detail_jabatan'] = $this->jabatan_model->detail($kode_jabatan);
		
		$validation = $this->form_validation;
		$validation->set_rules($this->jabatan_model->rules());
			
			if ($validation->run()){
				$this->jabatan_model->update($kode_jabatan);
				$this->session->set_flashdata('info', '<div style="color: green"> EDIT DATA BERHASIL! </div>');
				redirect("jabatan/index", "refresh");
		}
			
		//$this->load->view('input_karyawan', $data);
		$data['content'] = 'forms/Edit_jabatan';
			$this->load->view('home_2', $data);
	}
	
	public function delete($kode_jabatan)
	{
		$m_jabatan = $this->jabatan_model;
		$m_jabatan->delete($kode_jabatan);
		redirect("jabatan/index", "refresh");
	}
	
}